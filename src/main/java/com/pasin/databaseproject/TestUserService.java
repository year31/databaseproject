/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.databaseproject;

import com.pasin.databaseproject.model.User;
import com.pasin.databaseproject.service.UserService;

/**
 *
 * @author Pla
 */
public class TestUserService {

    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("user2", "password");
        if (user != null) {
            System.out.println("Welcome : " + user.getName());
        } else {
            System.out.println("Error");
        }
    }
}
